// import logo from "./logo.svg";
import "./App.css";
import React from "react";
import Rectangle from "./pages/rectangle";

function App() {
  return (
    <div className="App">
      <Rectangle />
    </div>
  );
}

export default App;
