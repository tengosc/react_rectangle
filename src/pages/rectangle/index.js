import React, { useRef, useState } from "react";
import RectangleResult from "./rectangleResult";
import styled from "styled-components";

const MyRectangle = styled.div`
  width: ${(props) => props.width + "px"};
  height: ${(props) => props.height + "px"};
  background-color: black;
  display: flex;
  text-align: center;
  align-items: center;
  justify-content: center;
  transition: all 0.5s;
`;

const Rectangle = () => {
  const [width, setWidth] = useState(103);
  const [height, setHeight] = useState(120);
  const max = 500;
  const min = 100;
  const myRef = useRef(null);

  const handleResize = () => {
    setWidth(Math.floor(Math.random() * (max - min)) + min);
    setHeight(Math.floor(Math.random() * (max - min)) + min);
  };

  return (
    <div className="rectangle__wrapper">
      <MyRectangle ref={myRef} width={width} height={height}>
        <RectangleResult {...{ myRef }} />
      </MyRectangle>
      <p>Szerokość: {width} px</p>
      <p>Wysokość: {height} px</p>
      <button onClick={() => handleResize()}>Generuj nowy element</button>
    </div>
  );
};

export default Rectangle;
